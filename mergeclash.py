import requests
import yaml
import os
from ruamel.yaml import YAML
from collections import OrderedDict

yaml = YAML()
yaml.indent(sequence=4, offset=2)

# 不重复地添加或覆盖节点
def add_or_replace_items(original_items, new_items):
    # 我们将使用名字作为键来查找相同的项目
    original_items_dict = {item['name']: item for item in original_items}
    
    # 迭代new_items并替换或添加到original_items_dict中
    for new_item in new_items:
        original_items_dict[new_item['name']] = new_item
    
    # 由于字典是无序的，我们需要按照new_items中的顺序来排列最终结果
    ordered_items = []
    original_names_set = set(original_items_dict.keys())
    for item in new_items:
        if item['name'] in original_names_set:
            ordered_items.append(original_items_dict[item['name']])
            original_names_set.remove(item['name'])
    # 保留原列表中现有但在new_items中没有提供的项目
    for name in original_items_dict:
        if name in original_names_set:
            ordered_items.append(original_items_dict[name])

    return ordered_items
# 排序节点名
def sort_proxy_names(proxies):
    def get_key(name):
        if 'warp' in name:
            prefix = '0'
        else:
            parts = name.rsplit('-', 1)  # 分割成协议和节点名称
            prefix = '1' + parts[-1] + parts[0] if len(parts) > 1 else '1' + name
        return prefix

    return sorted(proxies, key=get_key)

def update_proxy_groups(remote_proxy_groups, local_proxies_names):
    for group in remote_proxy_groups:
        # 获取当前组已经包含的代理
        current_group_set = set(group['proxies'])

        # 分别处理“🌍选择代理节点”组
        if group['name'] == '🌍选择代理节点':
            special_proxies = ['DIRECT', '负载均衡', '自动选择']
            special_proxies_set = set(special_proxies)

            # 从当前组中移除特殊代理，以便在列表前面重新添加它们
            group_proxies = [p for p in group['proxies'] if p not in special_proxies_set]

            # 从本地代理中添加缺失的代理，避免添加重复的代理
            new_proxies = [p for p in local_proxies_names if p not in current_group_set]

            # 排序后加回特殊代理
            updated_proxies = special_proxies + sort_proxy_names(group_proxies + new_proxies)
            group['proxies'] = list(OrderedDict.fromkeys(updated_proxies))

        else:
            # 对于其它组，只添加本地代理中缺失的代理，并进行排序
            new_proxies = [p for p in local_proxies_names if p not in current_group_set]
            group['proxies'] = sort_proxy_names(group['proxies'] + new_proxies)

# Function to download the file from GitLab and save as remote.yaml
def download_file_from_gitlab(url, headers, local_filename='remote.yaml'):
    response = requests.get(url, headers=headers)
    if response.status_code == 200:
        with open(local_filename, 'w', encoding='utf-8') as file:
            file.write(response.text)
    else:
        response.raise_for_status()

# Function to update the file on GitLab
def update_file_on_gitlab(url, headers, local_filename, branch, commit_message):
    with open(local_filename, 'r', encoding='utf-8') as file:
        content = file.read()
    payload = {
        'branch': branch,
        'commit_message': commit_message,
        'content': content,
        'encoding': 'text'
    }
    response = requests.put(url, headers=headers, data=payload)
    if response.status_code == 200 or response.status_code == 201:
        print("File updated successfully.")
    else:
        print("Failed to update file:", response.status_code, response.content)

# 合并 YAML 数据并进行所需的调整
def merge_yaml(local_file, remote_file):
    yaml = YAML()
    yaml.indent(mapping=2, sequence=4, offset=2)

    with open(local_file, 'r', encoding='utf-8') as lf:
        local_data = yaml.load(lf)
    with open(remote_file, 'r', encoding='utf-8') as rf:
        remote_data = yaml.load(rf)

    local_proxies_names = [proxy['name'] for proxy in local_data.get('proxies', [])]

    if 'proxies' in local_data and 'proxies' in remote_data:
        remote_data['proxies'] = add_or_replace_items(remote_data['proxies'], local_data['proxies'])

    if 'proxy-groups' in remote_data and 'proxy-groups' in local_data:
        update_proxy_groups(remote_data['proxy-groups'], local_proxies_names)

    with open(remote_file, 'w', encoding='utf-8') as rf:
        yaml.dump(remote_data, rf)

# Your GitLab personal access token
private_token = '???????????????'

# Headers for authentication
headers = {
    'PRIVATE-TOKEN': private_token
}

# URLs for GitLab API requests
gitlab_project_id = "???????????????"
gitlab_file_path = "clashmeta.yaml"
gitlab_raw_file_url = f"https://gitlab.com/api/v4/projects/{gitlab_project_id}/repository/files/{gitlab_file_path}/raw?ref=main"
gitlab_file_api_url = f"https://gitlab.com/api/v4/projects/{gitlab_project_id}/repository/files/{gitlab_file_path}"

# Download the remote YAML from GitLab
download_file_from_gitlab(gitlab_raw_file_url, headers)

# Merge local.yaml with remote.yaml
merge_yaml('/etc/s-box/clash_meta_client.yaml', 'remote.yaml')

# Push the updated 'remote.yaml' file back to GitLab
update_file_on_gitlab(
    gitlab_file_api_url,
    headers,
    local_filename='remote.yaml',
    branch='main',
    commit_message='Update clashmeta.yaml with merged content'
)

# Delete the 'remote.yaml' file after processing (if needed)
os.remove('remote.yaml')