import requests
import json
import os
import functools

# Define your local JSON data or alternatively load it from a file
local_data = {...}  # Insert your local JSON data here

# Your GitLab personal access token
private_token = '??????????'

# Headers for authentication
headers = {
    'PRIVATE-TOKEN': private_token
}

# URLs for GitLab API requests
gitlab_project_id = "??????????"
gitlab_file_path = "sing-box.json"
gitlab_raw_file_url = f"https://gitlab.com/api/v4/projects/{gitlab_project_id}/repository/files/{gitlab_file_path}/raw?ref=main"
gitlab_file_api_url = f"https://gitlab.com/api/v4/projects/{gitlab_project_id}/repository/files/{gitlab_file_path}"

def download_file_from_gitlab(url, headers, local_filename='remote.json'):
    response = requests.get(url, headers=headers)
    if response.status_code == 200:
        with open(local_filename, 'w', encoding='utf-8') as file:
            json.dump(response.json(), file, indent=4)
    else:
        response.raise_for_status()
    
def update_file_on_gitlab(url, headers, local_filename, branch, commit_message):
    with open(local_filename, 'r', encoding='utf-8') as file:
        content = file.read()
    payload = {
        'branch': branch,
        'commit_message': commit_message,
        'content': content,
        'encoding': 'text'
    }
    response = requests.put(url, headers=headers, data=payload)
    if response.status_code == 200 or response.status_code == 201:
        print(f"File updated successfully.")
    else:
        print(f"Failed to update file: {response.status_code}, {response.content}")

# Custom sort function for outbounds
def outbound_compare(item1, item2):
    if item1 == "auto":
        return -1
    if item2 == "auto":
        return 1
    if "warp" in item1 and "warp" not in item2:
        return -1
    if "warp" in item2 and "warp" not in item1:
        return 1

    # Extract the protocol and node name from the item
    protocol1, node1 = item1.split('-', 1) if '-' in item1 else (item1, '')
    protocol2, node2 = item2.split('-', 1) if '-' in item2 else (item2, '')

    # Sort by node name first, and then by protocol
    if node1 == node2:
        return (protocol1 > protocol2) - (protocol1 < protocol2)
    else:
        return (node1 > node2) - (node1 < node2)

def sort_outbounds(outbounds):
    sorted_outbounds = sorted(outbounds, key=lambda x: '0' if x == 'auto' else '1warp' if 'warp' in x else x)
    sorted_outbounds = sorted(sorted_outbounds, key=functools.cmp_to_key(outbound_compare))
    return sorted_outbounds

def merge_and_combine_outbounds(combine_type, combine_tag, remote_outbounds, local_outbounds):
    # Find the combined type outbound entry in both remote and local data
    remote_entry = next((ob for ob in remote_outbounds if ob['type'] == combine_type and ob['tag'] == combine_tag), None)
    local_entry = next((ob for ob in local_outbounds if ob['type'] == combine_type and ob['tag'] == combine_tag), None)
    
    # If both entries exist, combine the outbound lists without duplicating entries
    if remote_entry and local_entry:
        combined_outbounds = list(set(remote_entry['outbounds']) | set(local_entry['outbounds']))
        # After combining, sort the outbounds using the custom sort function
        remote_entry['outbounds'] = sort_outbounds(combined_outbounds)

def merge_json(local_file, remote_file):
    # Read the contents of the local and remote files
    with open(local_file, 'r', encoding='utf-8') as lf, open(remote_file, 'r') as rf:
        local_data = json.load(lf)
        remote_data = json.load(rf)

    # Process 'selector' type with 'select' tag
    merge_and_combine_outbounds('selector', 'select', remote_data['outbounds'], local_data['outbounds'])
    # Process 'urltest' type with 'auto' tag
    merge_and_combine_outbounds('urltest', 'auto', remote_data['outbounds'], local_data['outbounds'])

    # Replace other outbounds from local data, providing they do not have special types that we combine
    remote_outbounds_dict = {out['tag']: out for out in remote_data['outbounds'] if out['type'] not in ['selector', 'urltest']}

    for out in local_data['outbounds']:
        if out['type'] not in ['selector', 'urltest']:
            remote_outbounds_dict[out['tag']] = out
    
    # Combine the updated outbounds lists into the remote data
    remote_data['outbounds'] = list(remote_outbounds_dict.values()) + [out for out in remote_data['outbounds'] if out['type'] in ['selector', 'urltest']]

    # Write the merged data back to the remote file
    with open(remote_file, 'w', encoding='utf-8') as rf:
        json.dump(remote_data, rf, indent=4)

# Download the remote JSON from GitLab
download_file_from_gitlab(gitlab_raw_file_url, headers)

# Example usage, replace with the actual file paths
merge_json('/etc/s-box/sing_box_client.json', 'remote.json')

# Push the updated 'remote.json' file back to GitLab
update_file_on_gitlab(
    gitlab_file_api_url,
    headers,
    local_filename='remote.json',
    branch='main',
    commit_message='Update sing-box.json with merged content'
)

# Delete the 'remote.json' file
os.remove('remote.json')